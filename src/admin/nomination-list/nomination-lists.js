
import { fade } from "@material-ui/core/styles/colorManipulator";
const styles = (theme) => ({
search: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade("#EEEEEE", 1),
    "&:focus": {
      backgroundColor: fade("#f5f5f5", 1)

    },
    width:"200px",
    marginLeft:"975px"


  },
  searchIcon: {
    padding: theme.spacing.unit * 1,
    paddingLeft: theme.spacing.unit * 2
    
  },
inputRoot: {
    color: "inherit",
    height: "100%",
    width: "80%",
     paddingTop: theme.spacing.unit * 0.5
    
  },
  table:{
      marginRight:"50px",
      marginLeft:"50px",
      backgroundColor:""
  },
  tablehead:{
      backgroundColor:"#F2F3F4"
  },
  checkboxstyle:{
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",

  },
  checkboxalign:{
    display: "flex",
    flexDirection: "row",
    marginLeft:"50px"
    // padding:"20px"
    
  },
  textStyle:{
      marginTop:"10px",
  }
});
export default styles;