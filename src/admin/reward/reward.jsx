import React, { Component } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import '../dashboard/dashboard.scss';
import '../includes/header/header';
import '../includes/footer/footer';
import styles from './rewards';
import './reward.scss';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import Pagination from '@material-ui/lab/Pagination';
import { Paper, Table, Checkbox, TablePagination, TextField } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import PanoramaFishEyeIcon from '@material-ui/icons/PanoramaFishEye';
import Avatar from '@material-ui/core/Avatar';
import RadioButtonUncheckedRoundedIcon from '@material-ui/icons/RadioButtonUncheckedRounded';
import TableContainer from '@material-ui/core/TableContainer';

class Rewards extends Component {
   constructor(props) {
      super(props);
      this.state = {
         rows: [
            { Award: 'Thumps Up', rewardedBy: 'Jaron', date: '24-03-2020' },
            { Award: 'Masterio', rewardedBy: 'Asnad', date: '02-05-2020' },
            { Award: 'Hat Tip', rewardedBy: 'Vrinda', date: '15-07-2020' },
            { Award: 'Thumps Up', rewardedBy: 'Sayeed', date: '04-08-2020' },
         ],
         page: 0,
         rowsPerPage: 4,
      };
   }

   render() {
      const { classes } = this.props;
      const { rows, page, rowsPerPage } = this.state;

      return (
         <div className="main">
            <div className="reward">
               <Grid item>
                  <h2>My Reward List</h2>
               </Grid>
               <Paper
                  style={{
                     marginTop: '35px',
                     background: '#FFFFFF 0% 0% no-repeat padding-box',
                     boxShadow: '0px 3px 6px #AFB4C929',
                     opacity: '1',
                     paddingBottom: '20px',
                  }}
               >
                  <Grid item container xs={12}>
                     <Grid
                        container
                        direction="row"
                        style={{
                           marginLeft: '0px',
                           paddingTop: '12px',
                           paddingRight: '20px',
                           paddingBottom: '12px',
                           paddingLeft: '20px',
                        }}
                        className="nomination-list"
                        justify="space-between"
                        alignItems="center"
                     >
                        <Grid container className={classes.checkboxstyle}>
                           <Grid item md={0.5} className={classes.checkboxalign} style={{ marginLeft: '0' }}>
                              <Checkbox
                                 defaultChecked
                                 color="primary"
                                 inputProps={{
                                    'aria-label': 'checkbox with default color',
                                 }}
                              />
                              <Typography className={classes.textStyle}>ALL</Typography>
                           </Grid>
                           <Grid item md={1} className={classes.checkboxalign}>
                              <Checkbox
                                 defaultChecked
                                 color="primary"
                                 inputProps={{
                                    'aria-label': 'checkbox with default color',
                                 }}
                              />
                              <Typography className={classes.textStyle}>Rewards</Typography>
                           </Grid>
                           <Grid item md={1} className={classes.checkboxalign}>
                              <Checkbox
                                 defaultChecked
                                 color="primary"
                                 inputProps={{
                                    'aria-label': 'checkbox with default color',
                                 }}
                              />
                              <Typography className={classes.textStyle}>Appreciation</Typography>
                           </Grid>
                        </Grid>
                        <Grid className={classes.search} style={{ paddingTop: '1px' }}>
                           <TextField
                              fullWidth
                              placeholder="Search User"
                              className={classes.inputRoot}
                              name="searchText"
                              // value={searchText}
                              // onChange={this.handleSearchChange}
                              // onKeyDown={this.keyDown}
                              InputProps={{
                                 'aria-label': 'search',
                                 disableUnderline: true,
                              }}
                           />
                           <IconButton
                              className={classes.searchIcon}
                              aria-label="search"
                              onClick={(e) => {
                                 e.preventDefault();
                                 this.onSearchClickHandler();
                              }}
                           >
                              <SearchIcon />
                           </IconButton>
                        </Grid>
                     </Grid>
                     <br></br>
                  </Grid>
                  <div>
                     <Grid style={{ marginLeft: '20px', marginRight: '20px' }}>
                        <TableContainer>
                           <Table className="table" aria-label="simple table" style={{ marginTop: '1.0em' }}>
                              <TableHead
                                 className={classes.tablehead}
                                 style={{
                                    background: '#FFFFFF 0% 0% no-repeat padding-box',
                                 }}
                              >
                                 <TableRow style={{ marginTop: '1.0em' }}>
                                    <TableCell align="left" style={{ color: '#000000', width: '15%' }}>
                                       <b> Sl.no</b>
                                    </TableCell>
                                    <TableCell align="left" style={{ color: '#000000', width: '35%' }}>
                                       <b> Award </b>
                                    </TableCell>
                                    <TableCell align="left" style={{ color: '#000000', width: '35%' }}>
                                       <b> Rewarded By</b>
                                    </TableCell>
                                    <TableCell align="left" style={{ color: '#000000', width: '15%' }}>
                                       <b>Date </b>
                                    </TableCell>
                                 </TableRow>
                              </TableHead>
                              <TableBody>
                                 {rows &&
                                    rows.map((item, idx) => (
                                       <TableRow
                                          style={{
                                             top: '326px',
                                             marginTop: '1.0em',
                                             background: '#F5F6FA 0% 0% no-repeat padding-box',
                                             height: '50px',
                                             borderRadius: '5px',
                                             boxShadow: '0px 0px 3px #5A657B42',
                                             opacity: '0.62;',
                                             borderBottom: '4px solid #fff',
                                          }}
                                       >
                                          <TableCell align="left">
                                             <Typography>{idx + 1}</Typography>
                                          </TableCell>
                                          <TableCell align="left">
                                             <Grid container direction="row" alignItems="center">
                                                <Grid item style={{ marginRight: '10px', display: 'flex' }}>
                                                   <img
                                                      src={'./images/thumbsup.svg'}
                                                      style={{ width: '18px', height: '18px' }}
                                                      className="img-profile"
                                                   />
                                                </Grid>
                                                <Grid item>
                                                   <Typography>{item.Award}</Typography>
                                                </Grid>
                                             </Grid>
                                          </TableCell>
                                          <TableCell align="left">
                                             <Grid container direction="row" alignItems="center">
                                                <Grid item style={{ marginRight: '10px' }}>
                                                   <img
                                                      src={'./images/profile.png'}
                                                      style={{ width: '20px', height: '20px' }}
                                                   />
                                                </Grid>
                                                <Grid item>
                                                   <Typography>{item.rewardedBy}</Typography>
                                                </Grid>
                                             </Grid>
                                          </TableCell>
                                          <TableCell align="left">
                                             <Typography>{item.date}</Typography>
                                          </TableCell>
                                       </TableRow>
                                    ))}
                              </TableBody>
                           </Table>
                        </TableContainer>
                     </Grid>
                  </div>
               </Paper>

               <Pagination
                  shape="rounded"
                  count={10}
                  color="primary"
                  display="flex"
                  flexDirection="row"
                  justify="center"
                  className="w-100 pagination"
                  size="large"
               />
            </div>
         </div>
      );
   }
}
export const styler = withStyles(styles)(Rewards);
export default styler;
