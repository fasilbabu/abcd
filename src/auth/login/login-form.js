import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

class LoginForm extends Component {
   render() {
      return (
         <form className="form" noValidate autoComplete="off">
            <div className="form-ullogo">
               <img src="images/UL-Rewards-Logo.svg" />
            </div>
            <div className="form-group">
               <TextField
                  fullWidth
                  type="email"
                  label="Email"
                  variant="outlined"
                  InputProps={{
                     startAdornment: (
                        <InputAdornment position="start">
                           <PersonOutlineIcon />
                        </InputAdornment>
                     ),
                  }}
               />
            </div>

            <div className="form-group">
               <TextField
                  fullWidth
                  type="password"
                  label="Password"
                  variant="outlined"
                  InputProps={{
                     startAdornment: (
                        <InputAdornment position="start">
                           <LockOutlinedIcon />
                        </InputAdornment>
                     ),
                  }}
               />
            </div>
            <div className="form-forgot">
               <a>Forgot Password ?</a>
            </div>
            <div className="form-button-action">
               <Button fullWidth variant="contained" size="large" color="primary">
                  Login
               </Button>
            </div>
         </form>
      );
   }
}
export default LoginForm;
