import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import StepConnector from '@material-ui/core/StepConnector';
import Check from '@material-ui/icons/Check';
import Brightness1OutlinedIcon from '@material-ui/icons/Brightness1Outlined';
import SettingsIcon from '@material-ui/icons/Settings';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import VideoLabelIcon from '@material-ui/icons/VideoLabel';

function getSteps() {
   return ['Me', 'RA', 'Award Committee', 'HR'];
}

function getStepContent(step) {
   switch (step) {
      case 0:
         return 'Me';
      case 1:
         return 'RA';
      case 2:
         return 'Award Committee';
      case 3:
         return 'HR';
      default:
         return 'Unknown step';
   }
}

export default function NominationReturnStepper() {
   const useStyles = makeStyles((theme) => ({
      root: {
         width: '100%',
      },
      button: {
         marginRight: theme.spacing(1),
      },
      instructions: {
         marginTop: theme.spacing(1),
         marginBottom: theme.spacing(1),
      },
   }));
   const classes = useStyles();
   const [activeStep, setActiveStep] = React.useState(0);
   const [skipped, setSkipped] = React.useState(new Set());
   const steps = getSteps();

   const isStepOptional = (step) => {
      return step === 1;
   };

   const isStepSkipped = (step) => {
      return skipped.has(step);
   };

   const handleNext = () => {
      let newSkipped = skipped;
      if (isStepSkipped(activeStep)) {
         newSkipped = new Set(newSkipped.values());
         newSkipped.delete(activeStep);
      }

      setActiveStep((prevActiveStep) => prevActiveStep + 1);
      setSkipped(newSkipped);
   };

   const handleBack = () => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
   };

   const handleStep = (step) => () => {
      setActiveStep(step);
   };

   const handleSkip = () => {
      if (!isStepOptional(activeStep)) {
         // You probably want to guard against something like this,
         // it should never occur unless someone's actively trying to break something.
         throw new Error("You can't skip a step that isn't optional.");
      }

      setActiveStep((prevActiveStep) => prevActiveStep + 1);
      setSkipped((prevSkipped) => {
         const newSkipped = new Set(prevSkipped.values());
         newSkipped.add(activeStep);
         return newSkipped;
      });
   };

   const handleReset = () => {
      setActiveStep(0);
   };

   // connector
   const QontoConnector = withStyles({
      alternativeLabel: {
         top: 10,
         left: 'calc(-50% + 16px)',
         right: 'calc(50% + 16px)',
      },
      active: {
         '& $line': {
            borderColor: '#784af4',
         },
      },
      completed: {
         '& $line': {
            borderColor: '#784af4',
         },
      },
      line: {
         borderColor: '#eaeaf0',
         borderTopWidth: 3,
         borderRadius: 1,
      },
   })(StepConnector);

   const useQontoStepIconStyles = makeStyles({
      root: {
         color: '#fff',
         display: 'flex',
         height: 20,
         alignItems: 'center',

         borderRadius: '50%',
      },
      active: {
         color: '#fff',
         borderRadius: '50%',
         background: 'green',
         border: 'none',
      },
      circle: {
         color: '#fff',
         width: 20,
         height: 20,
         borderRadius: '50%',
         border: '4px solid gray',
      },
      completed: {
         color: '#fff',
         zIndex: 1,
         background: 'green',
         width: 20,
         height: 20,
         borderRadius: '50%',
      },
   });

   function QontoStepIcon(props) {
      const classes = useQontoStepIconStyles();
      const { active, completed } = props;

      return (
         <div
            className={clsx(classes.root, {
               [classes.active]: active,
            })}
         >
            {completed ? <Check className={classes.completed} /> : <div className={classes.circle} />}
         </div>
      );
   }

   QontoStepIcon.propTypes = {
      /**
       * Whether this step is active.
       */
      active: PropTypes.bool,
      /**
       * Mark the step as completed. Is passed to child components.
       */
      completed: PropTypes.bool,
   };

   // const ColorlibConnector = withStyles({
   //    alternativeLabel: {
   //       top: 22,
   //    },
   //    active: {
   //       '& $line': {
   //          backgroundImage: 'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
   //       },
   //    },
   //    completed: {
   //       '& $line': {
   //          backgroundImage: 'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
   //       },
   //    },
   //    line: {
   //       height: 3,
   //       border: 0,
   //       backgroundColor: '#eaeaf0',
   //       borderRadius: 1,
   //    },
   // })(StepConnector);

   const useColorlibStepIconStyles = makeStyles({
      root: {
         zIndex: 1,
         color: '#333',
         width: 50,
         height: 50,
         display: 'flex',
         borderRadius: '50%',
         justifyContent: 'center',
         alignItems: 'center',
         border: '2px solid gray',
      },
      active: {
         backgroundColor: 'green',
         borderColor: 'green',
         color: 'white',
         boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
      },
      completed: {
         backgroundColor: 'purple',
         borderColor: 'purple',
         color: 'white',
      },
   });

   function ColorlibStepIcon(props) {
      const classes = useColorlibStepIconStyles();
      const { active, completed } = props;

      const icons = {
         1: <SettingsIcon />,
         2: <GroupAddIcon />,
         3: <VideoLabelIcon />,
      };

      return (
         <div
            className={clsx(classes.root, {
               [classes.active]: active,
               [classes.completed]: completed,
            })}
         >
            {icons[String(props.icon)]}
         </div>
      );
   }

   ColorlibStepIcon.propTypes = {
      /**
       * Whether this step is active.
       */
      active: PropTypes.bool,
      /**
       * Mark the step as completed. Is passed to child components.
       */
      completed: PropTypes.bool,
      /**
       * The label displayed in the step icon.
       */
      icon: PropTypes.node,
   };

   return (
      <div className={classes.root}>
         {/* <Stepper activeStep={activeStep}>
            {steps.map((label, index) => {
               const stepProps = {};
               const labelProps = {};
               if (isStepOptional(index)) {
                  labelProps.optional = <Typography variant="caption"></Typography>;
               }
               if (isStepSkipped(index)) {
                  stepProps.completed = false;
               }
               return (
                  <Step onClick={handleStep(index)} key={label} {...stepProps}>
                     <StepLabel StepIconComponent={QontoStepIcon} {...labelProps}>{label}</StepLabel>
                  </Step>
               );
            })}
         </Stepper> */}

         <Stepper alternativeLabel activeStep={activeStep}>
            {steps.map((label, index) => (
               <Step onClick={handleStep(index)} key={label}>
                  <StepLabel StepIconComponent={QontoStepIcon}>{label}</StepLabel>
               </Step>
            ))}
         </Stepper>

         <div>
            <Typography className={classes.instructions}>All steps completed - you&apos;re finished</Typography>
         </div>
      </div>
   );
}
