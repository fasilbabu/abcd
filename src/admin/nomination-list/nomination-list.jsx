import React, { Component } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import './nomination-list.scss';
import '../includes/header/header';
import '../includes/footer/footer';
import styles from './nomination-lists';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import Pagination from '@material-ui/lab/Pagination';
import { Paper, Table, Checkbox, TablePagination, TextField } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import PanoramaFishEyeIcon from '@material-ui/icons/PanoramaFishEye';
import Avatar from '@material-ui/core/Avatar';
import RadioButtonUncheckedRoundedIcon from '@material-ui/icons/RadioButtonUncheckedRounded';
import TableContainer from '@material-ui/core/TableContainer';

class NominationList extends Component {
   constructor(props) {
      super(props);
      this.state = {
         rows: [
            { NominationId: '2342', NomineeName: 'Jaron', Award: 'Thumps Up', status: 'Pending' },
            { NominationId: '2343', NomineeName: 'Vrinda', Award: 'Thumps Up', status: 'Pending' },
            { NominationId: '2344', NomineeName: 'Arya', Award: 'Thumps Up', status: 'Pending' },
            { NominationId: '2345', NomineeName: 'Amal', Award: 'Thumps Up', status: 'Pending' },
         ],
         page: 0,
         rowsPerPage: 4,
      };
   }

   render() {
      const { classes } = this.props;
      const { rows, page, rowsPerPage } = this.state;

      return (
         <div className='main'>
            <div className='nominations'>
               <div className='nominations-title-head'>
                  <Grid item style={{}}>
                     <h2>My Nominations</h2>
                  </Grid>
               </div>
               <Paper
                  style={{
                     background: '#FFFFFF 0% 0% no-repeat padding-box',
                     boxShadow: '0px 3px 6px #AFB4C929',
                     opacity: '1',
                     paddingBottom: '20px',
                  }}
               >
                  <Grid container xs={12}>
                     <Grid
                        container
                        direction='row'
                        style={{
                           marginLeft: '0px',
                           paddingTop: '12px',
                           paddingRight: '20px',
                           paddingBottom: '12px',
                           paddingLeft: '20px',
                        }}
                        className='nomination-list'
                     >
                        <Grid container className={classes.checkboxstyle}>
                           <Grid item md={0.5} className={classes.checkboxalign}>
                              <Checkbox
                                 defaultChecked
                                 color='primary'
                                 inputProps={{ 'aria-label': 'checkbox with default color' }}
                              />
                              <Typography className={classes.textStyle}>ALL</Typography>
                           </Grid>
                           <Grid item md={1} className={classes.checkboxalign}>
                              <Checkbox
                                 defaultChecked
                                 color='primary'
                                 inputProps={{ 'aria-label': 'checkbox with default color' }}
                              />
                              <Typography className={classes.textStyle}>Approved</Typography>
                           </Grid>
                           <Grid item md={1} className={classes.checkboxalign}>
                              <Checkbox
                                 defaultChecked
                                 color='primary'
                                 inputProps={{ 'aria-label': 'checkbox with default color' }}
                              />
                              <Typography className={classes.textStyle}>Pending</Typography>
                           </Grid>
                           <Grid item md={1} className={classes.checkboxalign}>
                              <Checkbox
                                 defaultChecked
                                 color='primary'
                                 inputProps={{ 'aria-label': 'checkbox with default color' }}
                              />
                              <Typography className={classes.textStyle}>Returned</Typography>
                           </Grid>
                           <Grid item md={1} className={classes.checkboxalign}>
                              <Checkbox
                                 defaultChecked
                                 color='primary'
                                 inputProps={{ 'aria-label': 'checkbox with default color' }}
                              />
                              <Typography className={classes.textStyle}>Rejected</Typography>
                           </Grid>
                        </Grid>
                     </Grid>
                     <br></br>
                  </Grid>
                  <div>
                     <Grid style={{ paddingLeft: '0.5rem', paddingRight: '0.5rem' }}>
                        <TableContainer>
                           <Table className='table' aria-label='simple table' style={{ marginTop: '1.0em' }}>
                              <TableHead
                                 className={classes.tablehead}
                                 style={{ background: '#FFFFFF 0% 0% no-repeat padding-box' }}
                              >
                                 <TableRow style={{ marginTop: '1.0em' }}>
                                    <TableCell align='center' style={{ color: '#000000' }}>
                                       <b> Sl.no</b>
                                    </TableCell>{' '}
                                    <TableCell align='center' style={{ color: '#000000' }}>
                                       <b>Nomination ID</b>
                                    </TableCell>{' '}
                                    <TableCell align='center' style={{ color: '#000000' }}>
                                       <b> Nominee Name</b>
                                    </TableCell>{' '}
                                    <TableCell align='center' style={{ color: '#000000' }}>
                                       <b> Award </b>
                                    </TableCell>{' '}
                                    <TableCell align='center' style={{ color: '#000000' }}>
                                       <b>Status </b>
                                    </TableCell>{' '}
                                 </TableRow>
                              </TableHead>
                              <TableBody>
                                 {rows &&
                                    rows.map((item, idx) => (
                                       <TableRow
                                          style={{
                                             top: '326px',
                                             height: '50px',
                                             background: '#F5F6FA',
                                             borderRadius: '5px',
                                             opacity: '0.62;',
                                          }}
                                       >
                                          {/* onClick={handleClick}> */}

                                          <TableCell align='center'>
                                             <Typography>{idx + 1}</Typography>
                                          </TableCell>
                                          <TableCell align='center'>
                                             <Typography>{item.NominationId}</Typography>
                                          </TableCell>
                                          <TableCell align='center'>
                                             <Typography>{item.NomineeName}</Typography>
                                          </TableCell>
                                          <TableCell align='left'>
                                             <Grid container direction='row' alignItems='center'>
                                                <Grid item style={{ marginRight: '10px', display: 'flex' }}>
                                                   <img
                                                      src={'./images/thumbsup.svg'}
                                                      style={{ width: '18px', height: '18px' }}
                                                      className='img-profile'
                                                   />
                                                </Grid>
                                                <Grid item>
                                                   <Typography>{item.Award}</Typography>
                                                </Grid>
                                             </Grid>
                                          </TableCell>

                                          <TableCell align='left'>
                                             <Grid container direction='row' alignItems='center'>
                                                <Grid item style={{ marginRight: '10px' }}>
                                                   <img
                                                      src={'./images/Approved.svg'}
                                                      style={{ width: '22px', height: '22px' }}
                                                   />
                                                </Grid>
                                                <Grid item>
                                                   <Typography>{item.status}</Typography>
                                                </Grid>
                                             </Grid>
                                          </TableCell>
                                       </TableRow>
                                    ))}
                              </TableBody>
                           </Table>
                        </TableContainer>
                     </Grid>
                  </div>
               </Paper>
               <Pagination
                  shape='rounded'
                  count={10}
                  color='primary'
                  display='flex'
                  flexDirection='row'
                  justify='center'
                  className='w-100 pagination'
                  size='large'
               />
            </div>
         </div>
      );
   }
}
export const styler = withStyles(styles)(NominationList);
export default styler;
