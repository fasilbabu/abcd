import React, { Component } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import BreadCrumbs from "./detailsIndesx";
import { Grid } from '@material-ui/core';
import SearchIcon from "@material-ui/icons/Search";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import styles from "./statusDetailsStyle";
// import { push } from "connected-react-router";
import { connect } from "react-redux";
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import TablePagination from "@material-ui/core/TablePagination";
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/GetApp';
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import ToggleButton from "@material-ui/lab/ToggleButton";
import FormatAlignLeftIcon from '@material-ui/icons/FormatAlignLeft';
import FormatAlignCenterIcon from '@material-ui/icons/FormatAlignCenter';
import FormatAlignRightIcon from '@material-ui/icons/FormatAlignRight';
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import FormatBoldIcon from '@material-ui/icons/FormatBold';
import FormatItalicIcon from '@material-ui/icons/FormatItalic';
import FormatUnderlinedIcon from '@material-ui/icons/FormatUnderlined';
import FormatColorFillIcon from '@material-ui/icons/FormatColorFill';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import PublishIcon from '@material-ui/icons/Publish';
import Avatar from '@material-ui/core/Avatar';





class StatusDetails extends Component {
    constructor(props) {
        super(props);
        this.state = { activeStep:1,steps:["","RA","Award Committe","HR"] ,rejustification:"",bold:false }
    }
     
    rejustificationChange=(event)=>{this.setState({...this.state,rejustification:event.target.value})}
    bold=()=>{this.setState({...this.state,bold:!this.state.bold})}
    
    render() 
    { 
        const { classes } = this.props;
        const {activeStep,bold}= this.state;
        const {steps} = this.state;
        
       console.log(this.state.bold);
      
        return (  
            <div>
            <Grid container xs={12}>
            <Grid item xs={12} md={6}>
            <BreadCrumbs/>
          </Grid>
          
          <Grid item xs={12} md={6} >
         
      <Button variant="outlined" color="primary"  style={{marginRight:"50px"}} elevation={0}>
        Cancel
      </Button>
     
          <Button variant="contained" color="primary" style={{marginRight:"20px"}} elevation={0}>
        WithDraw
      </Button>
      <Button variant="contained" color="primary" style={{marginRight:"20px"}} elevation={0}>
        Submit
      </Button>
      </Grid>
      </Grid>
      <br></br>
      <Grid container xs={12}  >
      <Grid item xs={12} md={9}>
          <Paper  elevation={0}>
          <Stepper
          position="static"
        alternativeLabel
        activeStep={activeStep}
        // connector={<QontoConnector />}
      >
        {steps.map(label => (
          <Step key={label}>
            <StepLabel StepIconComponent={PersonPinCircleIcon}>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      
            <Paper   elevation={0}>
            <Grid>
                <Typography style={{fontSize:"20px"}}> <b> Comment </b></Typography>
                <Grid ><Avatar alt="Remy Sharp" src={"./images/images34.jpeg"} /> <b> Prakash Raj  </b> </Grid>
                </Grid>
                <br></br>

                <Grid xs={12} md={12}>
                <Typography style={{fontSize:"20px"}}> <b>Benefits Achieved </b> </Typography>
                <Grid > "Reward" is a song by English band the Teardrop Explodes. It was released as a single in early 1981 and is the band's biggest hit, peaking at No. 6 in the UK and No. 11 in Ireland.[1][2] The song was not initially included in the original 1980 UK & Europe releases of their debut album Kilimanjaro, but was included in the 1980 U.S. release together with the track "Suffocate" (replacing two tracks from the UK release). "Reward" was however added to later pressings of the album from 1981. </Grid>
        </Grid> 
        <br></br>
        <Grid ><Avatar alt="Remy Sharp" src={"./images/images34.jpeg"} /> <b> John Doe  </b> </Grid>
            
                <Grid>
                <Typography style={{fontSize:"20px"}}> <b> Rejustification </b></Typography>
                {/* <Grid  className={classes.textField}> Moreover, reference documentation should simultaneously describe every change . "Reward" is a song by English band the Teardrop Explodes. It was released as a single in early 1981 and is the band's biggest hit</Grid>
                 */}
                 <Grid> </Grid>
                 <br></br>
                 <Grid>
                 <ToggleButtonGroup exclusive aria-label="text alignment">
                 <ToggleButton value="left" aria-label="left aligned">
            <FormatAlignLeftIcon />
          </ToggleButton>
          <ToggleButton value="center" aria-label="centered">
            <FormatAlignCenterIcon />
          </ToggleButton>
          <ToggleButton value="right" aria-label="right aligned">
            <FormatAlignRightIcon />
          </ToggleButton>
          <ToggleButton value="bold" aria-label="bold" onClick={this.bold} >
            <FormatBoldIcon />
          </ToggleButton>
          <ToggleButton value="italic" aria-label="italic">
            <FormatItalicIcon />
          </ToggleButton>
          <ToggleButton value="underlined" aria-label="underlined">
            <FormatUnderlinedIcon />
          </ToggleButton>
          <ToggleButton >
          <input
        // accept="image/*"
        style={{display: 'none'}}
        id="contained-button-file"
        multiple
        type="file"
      />
      <label htmlFor="contained-button-file">
      <PublishIcon />
      </label>
            
          </ToggleButton>
                  
             
                </ToggleButtonGroup>
                </Grid>
                
                
                <TextField 
                style={{width:"900px"}}
                         variant="filled" multiline rows={4} 
                onChange={this.rejustificationChange}  value={this.state.rejustification} />
                
                
                </Grid>


            <ExpansionPanel >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
         
                <Typography style={{fontSize:"20px"}}> <b> Justification Details</b></Typography>
                <br></br>
                
          
        </ExpansionPanelSummary>
        <ExpansionPanelDetails >
        <Grid xs={12} md={12}>
                <Typography style={{fontSize:"20px"}}> <b> Justification </b></Typography>
                <Grid > "Reward" is a song by English band the Teardrop Explodes. It was released as a single in early 1981 and is the band's biggest hit, peaking at No. 6 in the UK and No. 11 in Ireland.[1][2] The song was not initially included in the original 1980 UK & Europe releases of their debut album Kilimanjaro, but was included in the 1980 U.S. release together with the track "Suffocate" (replacing two tracks from the UK release). "Reward" was however added to later pressings of the album from 1981...."Reward" is a song by English band the Teardrop Explodes. It was released as a single in early 1981 and is the band's biggest hit, peaking at No. 6 in the UK and No. 11 in Ireland.[1][2] The song was not initially included in the original 1980 UK & Europe releases of their debut album Kilimanjaro, but was included in the 1980 U.S. release together with the track "Suffocate" (replacing two tracks from the UK release). "Reward" was however added to later pressings of the album from 1981. </Grid>
                </Grid>
               
        </ExpansionPanelDetails>
        <ExpansionPanelDetails >
        
                <Grid xs={12} md={12}>
                <Typography style={{fontSize:"20px"}}> <b>Benefits Achieved </b> </Typography>
                <Grid > "Reward" is a song by English band the Teardrop Explodes. It was released as a single in early 1981 and is the band's biggest hit, peaking at No. 6 in the UK and No. 11 in Ireland.[1][2] The song was not initially included in the original 1980 UK & Europe releases of their debut album Kilimanjaro, but was included in the 1980 U.S. release together with the track "Suffocate" (replacing two tracks from the UK release). "Reward" was however added to later pressings of the album from 1981. </Grid>
        </Grid> 
        </ExpansionPanelDetails>
        <Divider />
        {/* <ExpansionPanelActions>
          <Button size="small">Cancel</Button>
          <Button size="small" color="primary">
            Save
          </Button>
        </ExpansionPanelActions> */}
      </ExpansionPanel>
             

                </Paper>

        
          </Paper>
        
        </Grid>
          <Grid item xs={12} md={3}>
          <Paper  elevation={0}>
            
              <Grid>
             
                <Typography> Nominee Name </Typography>
                <Grid> <Avatar alt="Remy Sharp" src="{./images/images34.jpg" /> <b> Jaron Jose Raj Joseph </b> </Grid>
                </Grid>
                <br></br>
                <Grid>
                <Typography> Award </Typography>
                <Grid> <Avatar alt="Remy Sharp" src="{./images/images34.jpg" /> <b> Thumps Up  </b> </Grid>
                </Grid>
                <br></br>
                <Grid>
                <Typography> Status </Typography>
                <Grid>  <Avatar alt="Remy Sharp" src="{./images/images34.jpg" /><b> Returned </b> </Grid>
                </Grid>
                <Grid>
                <Button
                elevation={0}
                        variant="contained"
                        color="default"
                     
                        startIcon={<CloudUploadIcon />}
                    >
                        File Download
                    </Button>
                </Grid>
          </Paper>
        
          </Grid>
      </Grid>
     
          </div>
        )
    }
}

// function mapDispatchToProps(dispach) {
//     return {
//       navigateTo: (url) => dispach(push(url)),
    
//     };
//   }
  // export const styler = withStyles(styles)(StatusDetails);
  
  // export default StatusDetails;
  export default connect(null, mapDispatchToProps)(styler);