import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import '../header/header.scss';

const Header = () => {
   const [anchorEl, setAnchorEl] = React.useState(null);

   const handleMenuClick = (event) => {
      setAnchorEl(event.currentTarget);
   };

   const handleMenuClose = () => {
      setAnchorEl(null);
   };

   return (
      <header className="header">
         <div className="header-logo">
            <img src={'./images/inner-page-logo.svg'} />
         </div>

         <div className="header-profile">
            <div className="m-l20 ">
               <img src={'./images/Settings.svg'} />
            </div>
            <div className="m-l20 ">
               <img src={'./images/noun_notification.svg'} />
            </div>

            <div className="header-profile-items">
               <div className="header-avatar-name">
                  <img src={'./images/profile.png'} />
               </div>

               <Button
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  onClick={handleMenuClick}
                  className="profile-name"
               >
                  John Doe
               </Button>
               <IconButton
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  onClick={handleMenuClick}
                  className="profile-name"
               >
                  {/* John Doe */}
                  <ExpandMoreIcon />
               </IconButton>
               <Menu
                  id="simple-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleMenuClose}
               >
                  <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
                  <MenuItem onClick={handleMenuClose}>My account</MenuItem>
                  <MenuItem onClick={handleMenuClose}>Logout</MenuItem>
               </Menu>
            </div>
         </div>
      </header>
   );
};

export default Header;
