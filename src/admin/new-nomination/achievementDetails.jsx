import React, { Component } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
// import BreadCrumbs from "./indexDetails";
import { Grid } from '@material-ui/core';
import SearchIcon from "@material-ui/icons/Search";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
// import styles from "./achievementDetailsStyle";
// import { push } from "connected-react-router";
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import TablePagination from "@material-ui/core/TablePagination";
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/GetApp';
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import ToggleButton from "@material-ui/lab/ToggleButton";
import FormatAlignLeftIcon from '@material-ui/icons/FormatAlignLeft';
import FormatAlignCenterIcon from '@material-ui/icons/FormatAlignCenter';
import FormatAlignRightIcon from '@material-ui/icons/FormatAlignRight';
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import FormatBoldIcon from '@material-ui/icons/FormatBold';
import FormatItalicIcon from '@material-ui/icons/FormatItalic';
import FormatUnderlinedIcon from '@material-ui/icons/FormatUnderlined';
import FormatColorFillIcon from '@material-ui/icons/FormatColorFill';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import PublishIcon from '@material-ui/icons/Publish';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputLabel from '@material-ui/core/InputLabel';





class AchivementDetails extends Component {
    constructor(props) {
        super(props);
        this.state = { activeStep:1,rejustification:"",bold:true }
    }
     
   handleChange = (event) => {
      const name = event.target.name;
      this.setState({
        ...this.state,
        [name]: event.target.value,
      });
    };

     
    rejustificationChange=(event)=>{this.setState({...this.state,rejustification:event.target.value})}
    bold=()=>{
      debugger
      this.setState({...this.state,bold:!this.state.bold})}
    
    render() 
    { 
        const { classes } = this.props;
        const {activeStep,bold}= this.state;
        const {steps} = this.state;
        
       console.log(this.state.bold);
       console.log()
      
        return (  
          <div className="main">
          <div className="new-nominations">
          <Grid container direction="row" justify="space-between" alignItems="center">
                  <Grid item style={{marginLeft:"60px"}}>
                     <h2>New Nomination</h2>
                  </Grid>
                  <Grid item>
                     <Grid item container direction="row" justify="flex-end" alignItems="center" wrap="nowrap">
                        <Button fullWidth variant="contained" size="large" color="primary" className="mr-l15">
                        Cancel

                        </Button>
                        <Button fullWidth variant="contained" size="large" color="secondary" className="mr-l15">
                           Submit
                        </Button>
                        {/* <Button fullWidth variant="contained" size="large" color="primary" className="mr-l15">
                           Approve
                        </Button> */}
                     </Grid>
                  </Grid>
               </Grid>
            </div>
            <Paper style={{marginLeft:"20px",marginTop:"10px",marginRight:"10px"}}>
            <div>
<Grid container xs={12}>
        <Grid item xs={12} md={6}>
        </Grid>
          
    
</Grid>
      <br></br>
<Grid container xs={12}>
      <Grid item xs={12} md={12}>
          <Paper  elevation={0}>
          
            <Paper   elevation={0}>
            <Grid>
                {/* <Typography style={{fontSize:"20px"}}> <b> New Nomination </b></Typography> */}
                {/* <Grid  className={classes.textField}> Moreover, reference documentation should simultaneously describe every change . "Reward" is a song by English band the Teardrop Explodes. It was released as a single in early 1981 and is the band's biggest hit</Grid>
                 */}
             </Grid>
      <Grid container xs={12} style={{marginTop:"10px",marginLeft:"60px"}}  >
        
      <FormControl variant="outlined" >
        <InputLabel htmlFor="outlined-age-native-simple" >Select Award</InputLabel>
        <Select
          native
          value={this.state.age}
          style={{width:"250px"}}
          onChange={this.handleChange}
          label="Select Award"
          inputProps={{
            name: 'age',
            id: 'outlined-age-native-simple',
          }}
        >
          <option aria-label="None" value="" />
          <option value={10}>Thumbsup</option>
          <option value={20}>Pinnacle</option>
          <option value={30}>Spark</option>
        </Select>
      </FormControl>

      <TextField
          id="outlined-textarea"
          label="Select Nominee"
          placeholder="Placeholder"
          multiline
          variant="outlined"
          style={{marginLeft:"50px",width:"250px"}}
        />
       
       </Grid>
                <br></br>
                <br></br>
                <Grid style={{marginLeft:"60px"}}>
                <Typography style={{fontSize:"20px"}}> <b> Justification </b></Typography>
                {/* <Grid  className={classes.textField}> Moreover, reference documentation should simultaneously describe every change . "Reward" is a song by English band the Teardrop Explodes. It was released as a single in early 1981 and is the band's biggest hit</Grid>
                 */}
                 <Grid> </Grid>
                 <br></br>
                 <Grid>
                 <ToggleButtonGroup   value={this.formats} onChange={this.handleFormat} exclusive aria-label="text alignment">
                 <ToggleButton value="left" aria-label="left aligned">
            <FormatAlignLeftIcon />
          </ToggleButton>
          <ToggleButton value="center" aria-label="centered">
            <FormatAlignCenterIcon />
          </ToggleButton>
          <ToggleButton value="right" aria-label="right aligned">
            <FormatAlignRightIcon />
          </ToggleButton>
          <ToggleButton value="bold" aria-label="bold" onClick={this.bold} >
            <FormatBoldIcon />
          </ToggleButton>
          <ToggleButton value="italic" aria-label="italic">
            <FormatItalicIcon />
          </ToggleButton>
          <ToggleButton value="underlined" aria-label="underlined">
            <FormatUnderlinedIcon />
          </ToggleButton>
          <ToggleButton >
          <input
        // accept="image/*"
        style={{display: 'none'}}
        id="contained-button-file"
        multiple
        type="file"
      />
      <label htmlFor="contained-button-file">
      <PublishIcon />
      </label>
            
          </ToggleButton>
                  
             
                </ToggleButtonGroup>
                </Grid>
                
                
                <TextField 
                style={{width:"1220px"}}
                         variant="filled" multiline rows={4} />
                 {/* onChange={this.rejustificationChange}  value={this.state.rejustification} /> */}
                <br></br>
                <br></br>
                <Grid>
                <Typography style={{fontSize:"20px"}}> <b> Benefits Achieved </b></Typography>
                {/* <Grid  className={classes.textField}> Moreover, reference documentation should simultaneously describe every change . "Reward" is a song by English band the Teardrop Explodes. It was released as a single in early 1981 and is the band's biggest hit</Grid>
                 */}
                 <Grid> </Grid>
                 <br></br>
                 <Grid>
                 <ToggleButtonGroup exclusive aria-label="text alignment">
                 <ToggleButton value="left" aria-label="left aligned">
            <FormatAlignLeftIcon />
          </ToggleButton>
          <ToggleButton value="center" aria-label="centered">
            <FormatAlignCenterIcon />
          </ToggleButton>
          <ToggleButton value="right" aria-label="right aligned">
            <FormatAlignRightIcon />
          </ToggleButton>
          <ToggleButton value="bold" aria-label="bold" onClick={this.bold} >
            <FormatBoldIcon />
          </ToggleButton>
          <ToggleButton value="italic" aria-label="italic">
            <FormatItalicIcon />
          </ToggleButton>
          <ToggleButton value="underlined" aria-label="underlined">
            <FormatUnderlinedIcon />
          </ToggleButton>
          <ToggleButton >
          <input
        // accept="image/*"
        style={{display: 'none'}}
        id="contained-button-file"
        multiple
        type="file"
      />
      <label htmlFor="contained-button-file">
      <PublishIcon />
      </label>
            
          </ToggleButton>
                  
             
                </ToggleButtonGroup>
                </Grid>
                
                
                <TextField 
                style={{width:"1220px"}}
                         variant="filled" multiline rows={4} 
                onChange={this.rejustificationChange}  value={this.state.rejustification} />
                <br></br>
                 
                </Grid>
                </Grid>


           
                </Paper>

        
          </Paper>
        
        </Grid>
 </Grid>
     
          </div>
          </Paper>
        
          </div>
          
        )
    }
}



export default AchivementDetails;