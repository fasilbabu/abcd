import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CloudUploadIcon from '@material-ui/icons/GetApp';
import '../team-nominations/team-nominations.scss';
import TeamNominationsStepper from './team-nominations-stepper';
import '../includes/header/header';
import '../includes/footer/footer';
import Confirmation  from './confirmation';

export default function TeamNominations() {
   return (
      <div className="main">
         <div className="team-nominations">
            <div className="team-nominations-title-head">
               <Grid container direction="row" justify="space-between" alignItems="center">
                  <Grid item>
                     <h2>Nomination ID: 4563</h2>
                  </Grid>
                  <Grid item>
                     <Grid item container direction="row" justify="flex-end" alignItems="center" wrap="nowrap">
                        <Button fullWidth variant="contained" size="large" color="primary" className="mr-l15">
                           Return
                        </Button>
                        <Button fullWidth variant="contained" size="large" color="primary" className="mr-l15">
                           Reject
                        </Button>
                        {/* <Button fullWidth variant="contained" size="large" color="primary" className="mr-l15">
                           Approve
                        </Button> */}
                        <Confirmation/>
                     </Grid>
                  </Grid>
               </Grid>
            </div>

            <Grid
               spacing={2}
               container
               direction="row"
               justify="center"
               alignItems="flex-start"
               spacing={4}
               className="bottom-card"
            >
               <Grid container item xs={12} sm={6} md={8} lg={8} xl={8}>
                  <Card className="nomination-card-large">
                     <div className="nomination-card-header">
                        <TeamNominationsStepper />
                     </div>
                     <div className="nomination-card-body">
                        <div className="nomination-comment">
                           {/* <h3>Comment</h3> */}

                           <Grid
                              container
                              direction="row"
                              justify="space-between"
                              alignItems="center"
                              className="nomination-list comment-container"
                           >
                              <Grid item container direction="row" justify="space-between" alignItems="center">
                                 {/* <Grid>
                                    <img src={'./images/nomini-1.png'} className="img-profile" />
                                    <p>Jaron Jose Raj Joseph</p>
                                 </Grid> */}
                                 <Grid>RA Approved on 21st of May 2020</Grid>
                              </Grid>
                           </Grid>
                        </div>
                        <div className="nomination-content">
                           <h3>Justification</h3>
                           <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                              been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                              a galley of type and scrambled it to make a type specimen book. It has in survived not
                              only five centuries, but also the leap into electronic typesetting, remaining essentially
                              unchanged. It was popularised a in the 1960s with the release of Letraset sheets
                              containing Lorem Ipsum passages.
                           </p>
                        </div>
                        <div className="nomination-content">
                           <h3>Benefits Achieved</h3>
                           <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                              been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                              a galley of type and scrambled it to make a type specimen book. It has in survived not
                              only five centuries, but also the leap into electronic typesetting, remaining essentially
                              unchanged. It was popularised a in the 1960s with the release of Letraset sheets
                              containing Lorem Ipsum passages.
                           </p>
                        </div>
                     </div>
                  </Card>
               </Grid>
               <Grid container item xs={12} sm={6} md={4} lg={4} xl={4}>
                  <Card className="nomination-card-small">
                     <CardContent>
                        <Grid container direction="row" alignItems="center" className="nomination-list">
                           <Grid>
                              <h5>Nominee Name</h5>
                           </Grid>
                           <Grid container direction="row" justify="flex-start" alignItems="center">
                              <img src={'./images/nomini-1.png'} className="img-profile" />
                              <p>Jaron Jose Raj Joseph</p>
                           </Grid>
                        </Grid>

                        <Grid container direction="row" alignItems="center" className="nomination-list">
                           <Grid>
                              <h5>Award</h5>
                           </Grid>
                           <Grid container direction="row" justify="flex-start" alignItems="center">
                              <img src={'./images/thumbsup.svg'} className="img-profile" />
                              <p>Thumps Up</p>
                           </Grid>
                        </Grid>

                        <Grid container direction="row" alignItems="center" className="nomination-list">
                           <Grid>
                              <h5>Status</h5>
                           </Grid>
                           <Grid container direction="row" justify="flex-start" alignItems="center">
                              <img src={'./images/Pending.svg'} className="img-profile" />
                              <p>Pending</p>
                           </Grid>
                          
                        </Grid>
                           <Grid>
                              <Button
                              style={{paddingTop:"2"}}
                              elevation={0}
                              variant="contained"
                              color="default"
                              className='teaminominations'
                              startIcon={<CloudUploadIcon />} >
                              File Download
                              </Button>
                           </Grid>
                     </CardContent>
                  </Card>
               </Grid>
            </Grid>
         </div>
      </div>
   );
}
