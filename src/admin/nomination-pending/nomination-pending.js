import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CloudUploadIcon from '@material-ui/icons/GetApp';
import '../nomination-pending/nomination-pending.scss';
import NominationPendingStepper from './nomination-pending-stepper';
import '../includes/header/header';
import '../includes/footer/footer';

export default function NominationPending() {
   return (
      <div className="main">
         <div className="nomination-pending">
            <div className="nomination-pending-title-head">
               <Grid container direction="row" justify="space-between" alignItems="center">
                  <Grid>
                     <h2>Nomination ID: 2342</h2>
                  </Grid>
                  <Grid item>
                     <Grid item container direction="row" justify="flex-end" alignItems="center" wrap="nowrap">
                        <Button fullWidth variant="contained" size="large" color="secondary" className="">
                           Cancel
                        </Button>
                        <Button fullWidth variant="contained" size="large" color="secondary" className="mr-l15">
                           Withdew
                        </Button>
                     </Grid>
                  </Grid>
               </Grid>
            </div>

            <Grid
               spacing={2}
               container
               direction="row"
               justify="center"
               alignItems="flex-start"
               spacing={4}
               className="bottom-card"
            >
               <Grid container item xs={12} sm={6} md={8} lg={8} xl={8}>
                  <Card className="nomination-pending-card-large">
                     <div className="nomination-pending-card-header">
                        <NominationPendingStepper />
                     </div>
                     <div className="nomination-pending-card-body">
                        <div className="nomination-pending-content">
                           <h3>Justification</h3>
                           <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                              been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                              a galley of type and scrambled it to make a type specimen book. It has in survived not
                              only five centuries, but also the leap into electronic typesetting, remaining essentially
                              unchanged. It was popularised a in the 1960s with the release of Letraset sheets
                              containing Lorem Ipsum passages.
                           </p>
                        </div>
                        <div className="nomination-pending-content">
                           <h3>Benefits Achieved</h3>
                           <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                              been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                              a galley of type and scrambled it to make a type specimen book. It has in survived not
                              only five centuries, but also the leap into electronic typesetting, remaining essentially
                              unchanged. It was popularised a in the 1960s with the release of Letraset sheets
                              containing Lorem Ipsum passages.
                           </p>
                        </div>
                     </div>
                  </Card>
               </Grid>
               <Grid container item xs={12} sm={6} md={4} lg={4} xl={4}>
                  <Card className="nomination-pending-card-small">
                     <CardContent>
                        <Grid container direction="row" alignItems="center" className="nomination-pending-list">
                           <Grid>
                              <h5>Nominee Name</h5>
                           </Grid>
                           <Grid container direction="row" justify="flex-start" alignItems="center">
                              <img src={'./images/nomini-1.png'} className="img-profile" />
                              <p>Jaron Jose Raj Joseph</p>
                           </Grid>
                        </Grid>

                        <Grid container direction="row" alignItems="center" className="nomination-pending-list">
                           <Grid>
                              <h5>Award</h5>
                           </Grid>
                           <Grid container direction="row" justify="flex-start" alignItems="center">
                              <img src={'./images/Pending.svg'} className="img-profile" />
                              <p>Thumbs Up</p>
                           </Grid>
                        </Grid>

                        <Grid container direction="row" alignItems="center" className="nomination-pending-list">
                           <Grid>
                              <h5>Status</h5>
                           </Grid>
                           <Grid container direction="row" justify="flex-start" alignItems="center">
                              <img src={'./images/Pending.svg'} className="img-profile" />
                              <p>Pending</p>
                           </Grid>
                        </Grid>
                        <Grid container direction="row" justify="flex-end" style={{ paddingTop: '25px' }}>
                           <Button
                              style={{ paddingTop: '2' }}
                              elevation={0}
                              variant="contained"
                              color="default"
                              className="teaminominations"
                              startIcon={<CloudUploadIcon />}
                           >
                              File Download
                           </Button>
                        </Grid>
                     </CardContent>
                  </Card>
               </Grid>
            </Grid>
         </div>
      </div>
   );
}
