import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import StarIcon from '@material-ui/icons/Star';
import '../dashboard/dashboard.scss';
import '../includes/header/header';
import '../includes/footer/footer';
import PieChart from './piechart';
import Badges from './badges';
import './dashboard.scss';

class Dashboard extends Component {
   constructor(props) {
      super(props);
      this.state = {
         projectStatus: [
            { name: 'Thumpus Up', value: 6 },
            { name: 'Masterro Award', value: 50 },
            { name: 'Pinnacle Award', value: 43 },
            { name: 'Hat Tip', value: 30 },
            { name: 'Spark Award', value: 10 },
         ],
      };
   }
   render() {
      const { classes } = this.props;
      const { projectStatus } = this.state;
      return (
         <div className='main'>
            <div className='dashboard'>
               <div>
                  <Badges />
                  <StarIcon />

                  <hr className='style-one mtb-45' />
               </div>
               <Grid spacing={2} container direction='row' justify='center' spacing={4} className='top-card'>
                  <Grid container item xs={12} sm={6} md={3} lg={3} xl={3}>
                     <Card className='dashboard-card'>
                        <CardContent>
                           <Grid container direction='row' justify='space-between' alignItems='center'>
                              <Grid>
                                 <h2>4</h2>
                                 <p variant='body2'>Approved</p>
                              </Grid>
                              <Grid>
                                 <img src={'./images/Approved.svg'} />
                              </Grid>
                           </Grid>
                        </CardContent>
                     </Card>
                  </Grid>
                  <Grid container item xs={12} sm={6} md={3} lg={3} xl={3}>
                     <Card className='dashboard-card'>
                        <CardContent>
                           <Grid container direction='row' justify='space-between' alignItems='center'>
                              <Grid>
                                 <h2>3</h2>
                                 <p variant='body2'>Pending</p>
                              </Grid>
                              <Grid>
                                 <img src={'./images/Pending.svg'} />
                              </Grid>
                           </Grid>
                        </CardContent>
                     </Card>
                  </Grid>

                  <Grid container item xs={12} sm={6} md={3} lg={3} xl={3}>
                     <Card className='dashboard-card'>
                        <CardContent>
                           <Grid container direction='row' justify='space-between' alignItems='center'>
                              <Grid>
                                 <h2>1</h2>
                                 <p variant='body2'>Returned</p>
                              </Grid>
                              <Grid>
                                 <img src={'./images/Returned.svg'} />
                              </Grid>
                           </Grid>
                        </CardContent>
                     </Card>
                  </Grid>

                  <Grid container item xs={12} sm={6} md={3} lg={3} xl={3}>
                     <Card className='dashboard-card'>
                        <CardContent>
                           <Grid container direction='row' justify='space-between' alignItems='center'>
                              <Grid>
                                 <h2>0</h2>
                                 <p variant='body2'>Rejected</p>
                              </Grid>
                              <Grid>
                                 {' '}
                                 <img src={'./images/Rejected.svg'} />
                              </Grid>
                           </Grid>
                        </CardContent>
                     </Card>
                  </Grid>
               </Grid>
               <Grid
                  spacing={2}
                  container
                  direction='row'
                  justify='center'
                  spacing={4}
                  className='dashboard-bottom-card'
               >
                  <Grid container item xs={12} sm={6} md={6} lg={6} xl={6}>
                     <Card className='dashboard-card-large dashboard-card-bottom-l'>
                        <div className='dashboard-chart-header'>
                           <h3>My Enrolement</h3>
                        </div>

                        <div className='dashboard-chart-body p-10 '>
                           <PieChart data={projectStatus} />
                        </div>
                     </Card>
                  </Grid>
                  <Grid container item xs={12} sm={6} md={6} lg={6} xl={6}>
                     <Card className='dashboard-card-large dashboard-card-bottom-r'>
                        <CardContent style={{ height: '100%', padding: '0' }}>
                           <Grid container direction='row' style={{ height: '100%' }}>
                              <Grid item xs={12} sm={12} md={6} lg={6} xl={6} className='flex-col-center border-r '>
                                 <img src={'./images/new-nomination-icon.svg'} className='m-b10' />
                                 <h3>New Nomination</h3>
                              </Grid>
                              <Grid item xs={12} sm={12} md={6} lg={6} xl={6} className='flex-col-center'>
                                 <img src={'./images/My-Award-Icon.svg'} className='m-b10' />
                                 <h3>My Award</h3>
                              </Grid>
                           </Grid>
                        </CardContent>
                     </Card>
                  </Grid>
               </Grid>
            </div>
         </div>
      );
   }
}

export default Dashboard;
