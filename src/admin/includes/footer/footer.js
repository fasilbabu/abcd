import React, { Component } from "react";
import "../footer/footer.scss";

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <p>© 2020 ULTS. All rights reserved.</p>
      </footer>
    );
  }
}

export default Footer;
