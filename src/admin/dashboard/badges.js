import React, { Component } from 'react';
import Box from '@material-ui/core/Box';

class Badges extends Component {
   constructor(props) {
      super(props);
   }

   render() {
      const gridWidth = {
         xs: '46%',
         sm: '46%',
         md: '18.2%',
         lg: '11.3%',
         xl: '11.3%',
      };

      return (
         <Box className='box' display='flex' flexDirection='row' flexWrap='wrap' justifyContent='space-between'>
            <Box className='box-item' width={gridWidth}>
               <div className='box-block'>
                  <img src={'./images/Thumbs Up.svg'} />
                  <h4 className='color-1 stars'>Thumbs Up</h4>
               </div>
            </Box>
            <Box className='box-item' width={gridWidth}>
               <div className='box-block'>
                  <img src={'./images/Spark.svg'} />
                  <h4 className='color-2'>Spark</h4>
               </div>
            </Box>
            <Box className='box-item' width={gridWidth}>
               <div className='box-block'>
                  <img src={'./images/Maestro.svg'} />
                  <h4 className='color-3'>Maestro</h4>
               </div>
            </Box>
            <Box className='box-item' width={gridWidth}>
               <div className='box-block'>
                  <img src={'./images/Pinnacle.svg'} />
                  <h4 className='color-4'>Pinnacle</h4>
               </div>
            </Box>
            <Box className='box-item' width={gridWidth}>
               <div className='box-block'>
                  <img src={'./images/Hat Tip.svg'} />
                  <h4 className='color-5'>Hat Tip</h4>
               </div>
            </Box>
            <Box className='box-item' width={gridWidth}>
               <div className='box-block'>
                  <img src={'./images/Ulite_Bronze.svg'} />
                  <h4 className='color-6'>Ulite Bronze</h4>
                  <p className='Bronze'>3 years</p>
               </div>
            </Box>
            <Box className='box-item' width={gridWidth}>
               <div className='box-block'>
                  <img src={'./images/Ulite_Silver.svg'} />
                  <h4 className='color-7'>Ulite Silver</h4>
                  <p className='Silver'>5 years</p>
               </div>
            </Box>
            <Box className='box-item' width={gridWidth}>
               <div className='box-block'>
                  <img src={'./images/Ulite_Gold.svg'} />
                  <h4 className='color-8'>Ulite Gold</h4>
                  <p className='Gold'>10 years</p>
               </div>
            </Box>
         </Box>
         //<div className="style-one mtb-45"></div>
      );
   }
}

export default Badges;
