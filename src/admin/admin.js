import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Header from "./includes/header/header";

class Admin extends Component {
  render() {
    return (
      <div className="admin">
        <Header />
      </div>
    );
  }
}

export default Admin;
