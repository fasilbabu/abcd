import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
// import ToggleButton from "@material-ui/lab/ToggleButton";
// import FormatAlignLeftIcon from '@material-ui/icons/FormatAlignLeft';
// import FormatAlignCenterIcon from '@material-ui/icons/FormatAlignCenter';
// import FormatAlignRightIcon from '@material-ui/icons/FormatAlignRight';
// import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
// import FormatBoldIcon from '@material-ui/icons/FormatBold';
// import FormatItalicIcon from '@material-ui/icons/FormatItalic';
// import FormatUnderlinedIcon from '@material-ui/icons/FormatUnderlined';
// import FormatColorFillIcon from '@material-ui/icons/FormatColorFill';
// import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
// import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
// import PublishIcon from '@material-ui/icons/Publish';
import {Grid} from '@material-ui/core';


export default function FormDialog() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button fullWidth variant="contained" size="large" color="primary" className="mr-l15" onClick={handleClickOpen} style={{marginRight:"20px"}} elevation={0}>
APPROVE      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Submit Confirmation</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates
            occasionally.
          </DialogContentText> */}

<Grid>
                 {/* <ToggleButtonGroup exclusive aria-label="text alignment">
                 <ToggleButton value="left" aria-label="left aligned">
            <FormatAlignLeftIcon />
          </ToggleButton>
          <ToggleButton value="center" aria-label="centered">
            <FormatAlignCenterIcon />
          </ToggleButton>
          <ToggleButton value="right" aria-label="right aligned">
            <FormatAlignRightIcon />
          </ToggleButton>
          {/* <ToggleButton value="bold" aria-label="bold" onClick={this.bold} >
            <FormatBoldIcon />
          </ToggleButton> */}
          {/* <ToggleButton value="italic" aria-label="italic">
            <FormatItalicIcon />
          </ToggleButton>
          <ToggleButton value="underlined" aria-label="underlined">
            <FormatUnderlinedIcon />
          </ToggleButton>
          <ToggleButton >
          <input */}
         {/* accept="image/*"
        style={{display: 'none'}}
        id="contained-button-file"
        multiple
        type="file"
      /> */}
      {/* <label htmlFor="contained-button-file">
      <PublishIcon />
      </label>
            
          </ToggleButton>
                  
             
                </ToggleButtonGroup>  */}
                </Grid>
                {/* <Grid container item xs={12} sm={6} md={4} lg={4} xl={4}> */}
                  <Card className="nomination-card-large">
                     <CardContent>
                <TextField 
                style={{width:"400px"}}
                label="Type your comments"
                 multiline rows={4} />
                     </CardContent>
                     </Card>
                     {/* </Grid> */}

          {/* <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Comment"
            type="email"
            fullWidth
          /> */}
        </DialogContent>
        <DialogActions>
        <Button fullWidth variant="contained" size="small"  className="mr-l15"onClick={handleClose} color="secondary">
            CANCEL
          </Button>
          <Button fullWidth variant="contained" size="small" className="mr-l15"onClick={handleClose} color="primary">
            CONFIRM
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
