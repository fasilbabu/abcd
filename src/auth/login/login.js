import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import LoginForm from "./login-form";
import "./login.scss";

class Login extends Component {
  render() {
    return (
      <Grid
        className="login"
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid container item xs={12} sm={12} md={4} lg={4} xl={5}>
          <div className="login-form">
            <LoginForm />
          </div>
        </Grid>

        <Grid container item xs={12} sm={12} md={7} lg={6} xl={5}>
          <div className="login-graphic-layout">
            <div className="login-graphic">
              <img src={"./images/login-illustration.svg"} />
            </div>
            <div className="copyright">
              <span>&copy; 2020 ULTS All rights received.</span>
            </div>
          </div>
        </Grid>
      </Grid>
    );
  }
}

export default Login;
