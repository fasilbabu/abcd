import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './auth/login/login';
import Admin from './admin/admin';
import Dashboard from './admin/dashboard/dashboard';
import Nomination from './admin/nomination/nomination';
import NominationPending from './admin/nomination-pending/nomination-pending';
import TeamNominations from './admin/team-nominations/team-nominations';
import Reward from './admin/reward/reward';
import NewNomination from './admin/new-nomination/achievementDetails';


class AppRouter extends Component {
   render() {
      return (
         <Router>
            <Switch>
               <Route exact path="/" component={Login}></Route>
               <Route path="/login" component={Login}></Route>
               <Route path="/admin" component={Admin}></Route>
               <Route path="/dashboard" component={Dashboard}></Route>
               <Route path="/nomination" component={Nomination}></Route>
               <Route path="/nomination-pending" Component={NominationPending}></Route>
               <Route path="/teamnominations" Component={TeamNominations}></Route>
               <Route path="/rewards" Component={Reward}></Route>
               <Route path="/new-nomination" Component={NewNomination}></Route>
            </Switch>
         </Router>
      );
   }
}

export default AppRouter;
