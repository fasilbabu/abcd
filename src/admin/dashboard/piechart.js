import React, { Component } from 'react';
import { ResponsiveContainer, PieChart, Pie, Sector, Cell, Legend, Tooltip } from 'recharts';

const COLORS = ['#C5A3FF', '#FCC2FF', '#B5EAD7', '#FFB7B2', '#77E59B'];

const renderActiveShape = (props) => {
   const RADIAN = Math.PI / 180;
   const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
   const sin = Math.sin(-RADIAN * midAngle);
   const cos = Math.cos(-RADIAN * midAngle);
   const sx = cx + (outerRadius + 30) * cos;
   const sy = cy + (outerRadius + 10) * sin;
   const mx = cx + (outerRadius + 30) * cos;
   const my = cy + (outerRadius + 30) * sin;
   const ex = mx + (cos >= 0 ? 1 : -1) * 22;
   const ey = my;
   const textAnchor = cos >= 0 ? 'start' : 'end';

   return (
      <g>
         <text x={cx} y={cy} dy={8} textAnchor="middle" fill="#000">
            {payload.name}
         </text>

         <text x={cx} y={cy + 15} dy={8} textAnchor="middle" fontSize={12} fill="#97839F">
            {value}
            {value !== 1 ? ' Vistors' : ' Visitor'}
         </text>
         <Sector
            cx={cx}
            cy={cy}
            innerRadius={innerRadius}
            outerRadius={outerRadius}
            startAngle={startAngle}
            endAngle={endAngle}
            fill={fill}
         />
         <Sector
            cx={cx}
            cy={cy}
            startAngle={startAngle}
            endAngle={endAngle}
            innerRadius={outerRadius + 6}
            outerRadius={outerRadius + 10}
            fill={fill}
         />
      </g>
   );
};

class Chart extends Component {
   constructor(props) {
      super(props);
      this.state = { activeIndex: 0 };
   }

   onPieEnter = (data, index) => {
      this.setState({
         activeIndex: index,
      });
   };
   render() {
      const { data } = this.props;
      return (
         <PieChart width={360} height={320}>
            <Pie
               data={data}
               labelLine={false}
               paddingAngle={1}
               innerRadius={70}
               outerRadius={133}
               fill="#8884d8"
               dataKey="value"
            >
               {data.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
               ))}
            </Pie>
            <Tooltip />
            <Legend layout="vetical" verticalAlign="middle" align="right" />
         </PieChart>
      );
   }
}

export default Chart;
