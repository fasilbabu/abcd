import React, { Component } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import BreadCrumbs from "./indexDetails";
import { Grid } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
// import styles from "./achievementDetailsStyle";
// import { push } from "connected-react-router";
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import TablePagination from '@material-ui/core/TablePagination';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/GetApp';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import ToggleButton from '@material-ui/lab/ToggleButton';
import FormatAlignLeftIcon from '@material-ui/icons/FormatAlignLeft';
import FormatAlignCenterIcon from '@material-ui/icons/FormatAlignCenter';
import FormatAlignRightIcon from '@material-ui/icons/FormatAlignRight';
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import FormatBoldIcon from '@material-ui/icons/FormatBold';
import FormatItalicIcon from '@material-ui/icons/FormatItalic';
import FormatUnderlinedIcon from '@material-ui/icons/FormatUnderlined';
import FormatColorFillIcon from '@material-ui/icons/FormatColorFill';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import PublishIcon from '@material-ui/icons/Publish';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputLabel from '@material-ui/core/InputLabel';
import NominationReturnStepper from './nomination-return-stepper';
import '../nomination-return/nomination-return.scss';

class NominationReturn extends Component {
   constructor(props) {
      super(props);
      this.state = { activeStep: 1, rejustification: '', bold: true };
   }

   handleChange = (event) => {
      const name = event.target.name;
      this.setState({
         ...this.state,
         [name]: event.target.value,
      });
   };
   rejustificationChange = (event) => {
      this.setState({ ...this.state, rejustification: event.target.value });
   };
   bold = () => {
      debugger;
      this.setState({ ...this.state, bold: !this.state.bold });
   };

   render() {
      const { classes } = this.props;
      const { activeStep, bold } = this.state;
      const { steps } = this.state;
      console.log(this.state.bold);
      console.log();
      return (
         <div className="main">
            <div className="nomination-return">
               <div className="nomination-return-title-head">
                  <Grid container direction="row" justify="space-between" alignItems="center">
                     <Grid item style={{ marginLeft: '5px' }}>
                        <h2>Nomination ID:4563</h2>
                     </Grid>
                     <Grid item>
                        <Grid item container direction="row" justify="flex-end" alignItems="center" wrap="nowrap">
                           <Button fullWidth variant="contained" size="large" color="secondary" className="">
                              Cancel
                           </Button>
                           <Button fullWidth variant="contained" size="large" color="secondary" className="mr-l15">
                              Withdew
                           </Button>
                           <Button fullWidth variant="contained" size="large" color="primary" className="mr-l15">
                              Submit
                           </Button>
                        </Grid>
                     </Grid>
                  </Grid>
               </div>
               <Grid
                  spacing={2}
                  container
                  direction="row"
                  justify="center"
                  alignItems="flex-start"
                  spacing={4}
                  className="bottom-card"
               >
                  <Grid container item xs={12} sm={6} md={8} lg={8} xl={8}>
                     <Card className="nomination-return-card-large">
                        <div className="nomination-return-card-header">
                           <NominationReturnStepper />
                        </div>
                        <div className="nomination-return-card-body">
                           <div className="nomination-return-content">
                              <h3>Comment</h3>

                              <Grid
                                 container
                                 direction="row"
                                 justify="space-between"
                                 alignItems="center"
                                 className="nomination-return-comment-list nomination-return-container"
                              >
                                 <Grid item container direction="row" justify="space-between" alignItems="center">
                                    <Grid item container item xs={12} sm={12} md={6} lg={6} xl={6}>
                                       <img src={'./images/nomini-1.png'} className="img-comment" />
                                       <span className="commet-details">
                                          <h4>Jaron Jose Raj Joseph</h4>
                                          <p>Manager</p>
                                       </span>
                                    </Grid>
                                    <Grid item item xs={12} sm={12} md={6} lg={6} xl={6} className="align-R">
                                       <div className="calendar">
                                          <p>May 18, 2020</p>
                                          <p>9:17 PM</p>
                                       </div>
                                    </Grid>
                                 </Grid>
                                 <hr className="style-one mtb-15" />
                                 <Grid>
                                    <p>
                                       Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                       Ipsum has been the industry's standard of dummy text ever since the 1500s, when
                                       an unknown printer took a galley.
                                    </p>
                                 </Grid>
                              </Grid>

                              <Grid
                                 container
                                 direction="row"
                                 justify="space-between"
                                 alignItems="center"
                                 className="nomination-return-comment-list nomination-return-container mt-15"
                              >
                                 <Grid item container direction="row" justify="space-between" alignItems="center">
                                    <Grid item container item xs={12} sm={12} md={6} lg={6} xl={6}>
                                       <img src={'./images/nomini-1.png'} className="img-comment" />
                                       <span className="commet-details">
                                          <h4>Jaron Jose Raj Joseph</h4>
                                          <p>Manager</p>
                                       </span>
                                    </Grid>
                                    <Grid item item xs={12} sm={12} md={6} lg={6} xl={6} className="align-R">
                                       <div className="calendar">
                                          <p> May 18, 2020</p>
                                          <p>9:17 PM</p>
                                       </div>
                                    </Grid>
                                 </Grid>
                              </Grid>
                           </div>
                           <div className="nomination-return-content">
                              <Typography style={{ fontSize: '20px' }}>
                                 {' '}
                                 <h3> Rejustification </h3>
                              </Typography>
                              <div className="border-outer">
                                 <ToggleButtonGroup
                                    value={this.formats}
                                    onChange={this.handleFormat}
                                    exclusive
                                    aria-label="text alignment"
                                 >
                                    <ToggleButton value="left" aria-label="left aligned">
                                       <FormatAlignLeftIcon />
                                    </ToggleButton>
                                    <ToggleButton value="center" aria-label="centered">
                                       <FormatAlignCenterIcon />
                                    </ToggleButton>
                                    <ToggleButton value="right" aria-label="right aligned">
                                       <FormatAlignRightIcon />
                                    </ToggleButton>
                                    <ToggleButton value="bold" aria-label="bold" onClick={this.bold}>
                                       <FormatBoldIcon />
                                    </ToggleButton>
                                    <ToggleButton value="italic" aria-label="italic">
                                       <FormatItalicIcon />
                                    </ToggleButton>
                                    <ToggleButton value="underlined" aria-label="underlined">
                                       <FormatUnderlinedIcon />
                                    </ToggleButton>
                                    <ToggleButton>
                                       <input
                                          // accept="image/*"
                                          style={{ display: 'none' }}
                                          id="contained-button-file"
                                          multiple
                                          type="file"
                                       />
                                       <label htmlFor="contained-button-file">
                                          <PublishIcon />
                                       </label>
                                    </ToggleButton>
                                 </ToggleButtonGroup>
                                 <TextField
                                    style={{ width: '100%' }}
                                    variant="filled"
                                    multiline
                                    rows={4}
                                    onChange={this.rejustificationChange}
                                    value={this.state.rejustification}
                                 />
                              </div>
                           </div>
                           <div className="nomination-return-content">
                              <h3>Justification</h3>
                              <p>
                                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                 has been the industry's standard dummy text ever since the 1500s, when an unknown
                                 printer took a galley of type and scrambled it to make a type specimen book. It has in
                                 survived not only five centuries, but also the leap into electronic typesetting,
                                 remaining essentially unchanged. It was popularised a in the 1960s with the release of
                                 Letraset sheets containing Lorem Ipsum passages.
                              </p>
                           </div>
                           <div className="nomination-return-content">
                              <h3>Benefits Achieved</h3>
                              <p>
                                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                 has been the industry's standard dummy text ever since the 1500s, when an unknown
                                 printer took a galley of type and scrambled it to make a type specimen book. It has in
                                 survived not only five centuries, but also the leap into electronic typesetting,
                                 remaining essentially unchanged. It was popularised a in the 1960s with the release of
                                 Letraset sheets containing Lorem Ipsum passages.
                              </p>
                           </div>
                        </div>
                     </Card>
                  </Grid>
                  <Grid container item xs={12} sm={6} md={4} lg={4} xl={4}>
                     <Card className="nomination-return-card-small">
                        <CardContent>
                           <Grid container direction="row" alignItems="center" className="nomination-return-list">
                              <Grid>
                                 <h5>Nominee Name</h5>
                              </Grid>
                              <Grid container direction="row" justify="flex-start" alignItems="center">
                                 <img src={'./images/nomini-1.png'} className="img-profile" />
                                 <p>Jaron Jose Raj Joseph</p>
                              </Grid>
                           </Grid>

                           <Grid container direction="row" alignItems="center" className="nomination-return-list">
                              <Grid>
                                 <h5>Award</h5>
                              </Grid>
                              <Grid container direction="row" justify="flex-start" alignItems="center">
                                 <img src={'./images/Pending.svg'} className="img-profile" />
                                 <p>Thumbs Up</p>
                              </Grid>
                           </Grid>

                           <Grid container direction="row" alignItems="center" className="nomination-return-list">
                              <Grid>
                                 <h5>Status</h5>
                              </Grid>
                              <Grid container direction="row" justify="flex-start" alignItems="center">
                                 <img src={'./images/Returned.svg'} className="img-profile" />
                                 <p>Return</p>
                              </Grid>
                           </Grid>
                           <Grid container direction="row" justify="flex-end" style={{ paddingTop: '25px' }}>
                              <Button
                                 style={{ paddingTop: '2' }}
                                 elevation={0}
                                 variant="contained"
                                 color="secondary"
                                 className="teaminominations"
                                 startIcon={<CloudUploadIcon />}
                              >
                                 File Download
                              </Button>
                           </Grid>
                        </CardContent>
                     </Card>
                  </Grid>
               </Grid>
            </div>
         </div>
      );
   }
}
export default NominationReturn;
