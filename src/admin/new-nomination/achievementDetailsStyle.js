const styles = () => ({
    button:{
        //  marginRight:"2px",
        marginTop:"20px",
        display: "flex",
        // flexDirection: "row",
        justifyContent: "flex-end",
        elevation:0
    },
    textField:{
       // border: "1px solid black",
        // marginRight:"20px",
        // marginLeft:"20px",
        padding:"20px"

    },
    leftboxStyle:{
        marginRight:"30px",
        marginLeft:"30px",
        padding:"25px",
        elevation:0,
        // height:"65vh",
        // overflow: "auto"
    },

    textStyle:{
        elevation:0,
        // height:"60vh",
        overflow: "auto"

    },

    rightboxStyle:{
        marginRight:"30px",
        marginLeft:"10px",
        padding:"25px",
        elevation:0
    },
    containerStyle:{
        // marginRight:"30px",
        // marginLeft:"30px"

    },
    boldtextType:{
        fontWeight:"200px",
        color:"red",
        width:"900px"
    },
    textType:{
        color:"green"
    },
    input: {
        display: 'none',
      },
    

})
export default styles;