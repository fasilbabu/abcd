import React from 'react';
import Button from '@material-ui/core/Button';
import Login from './auth/login/login';
import Header from './admin/includes/header/header';
import Footer from './admin/includes/footer/footer';
import Dashboard from './admin/dashboard/dashboard';
import Nomination from './admin/nomination/nomination';
import NominationPending from './admin/nomination-pending/nomination-pending';
import TeamNominations from './admin/team-nominations/team-nominations';
import NewNomination from './admin/new-nomination/achievementDetails';
import Reward from './admin/reward/reward';
import AppRouter from './app-router';
import NominationList from './admin/nomination-list/nomination-list';
import NominationReturn from './admin/nomination-return/nomination-return';

function App() {
   return (
      <div className="App">
         <Header />
         {/* <NominationReturn /> */}
         {/* <NominationPending /> */}
         <NominationList />
         {/* <NewNomination /> */}
         {/* <TeamNominations /> */}
         {/* <Reward /> */}
         {/* <AppRouter /> */}
         {/* <Dashboard /> */}
         <Footer />
      </div>
   );
}

export default App;
